set enc=utf-8
syntax on
set number relativenumber
autocmd FileType python setlocal tabstop=4
set expandtab!
